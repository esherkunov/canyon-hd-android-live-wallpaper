package ru.atonica.canyon;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.Preference.OnPreferenceClickListener;

public class CanyonWallpaperPrefs extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
		addPreferencesFromResource(R.xml.walpaper_prefs);
		
		Preference rateThis = (Preference)findPreference("rate_this");
		rateThis.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=ru.atonica.canyon"));
				startActivity(intent);
				return true;
			}
		});
		Preference shareThis = (Preference)findPreference("share_this");
		shareThis.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_SUBJECT, "Grand Canyon HD live wallpaper");
				intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=ru.atonica.canyon");
				startActivity(Intent.createChooser(intent, "Share via"));
				return true;
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
		super.onDestroy();
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		
	}

}
