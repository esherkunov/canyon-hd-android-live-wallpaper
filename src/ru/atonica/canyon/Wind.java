package ru.atonica.canyon;

public class Wind {
	public static enum WindDirection {LEFT, RIGHT};
	public float speed;
	public WindDirection windDirection;
	public Wind(float speed, WindDirection windDirection) {
		this.speed = speed;
		this.windDirection = windDirection;
	}
	
}
