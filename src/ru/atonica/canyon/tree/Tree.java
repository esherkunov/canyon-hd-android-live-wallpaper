package ru.atonica.canyon.tree;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import ru.atonica.canyon.Constants;
import ru.atonica.canyon.Wind;

public class Tree extends Sprite {
	
	private Wind wind;
	private int leafCount;
	private float x, y;
	private LeafTexture[] leafTexture;
	private TreeBranch[] treeBranch = TreeBranch.fromArray(new float[][] {
		{338, 125},
		{401, 128},
		{491, 114},
		{415, 236},
		{347, 162},
		{388, 271},
		{275, 79},
		{209, 50},
		{187, 158},
		{409, 305},
		{370, 326},
		{367, 294},
		{313, 311},
		{311, 279},
		{215, 199},
		{469, 170}
	});
	private Leaf[] leaves;
	private VertexBufferObjectManager vertexBufferObjectManager;

	public Tree(float x, float y, ITextureRegion textureRegion, VertexBufferObjectManager vertexBufferObjectManager, Wind wind, LeafTexture[] leafTexture, int leafCount) {
		super(x, y, textureRegion, vertexBufferObjectManager);
		this.x = x;
		this.y = y;
		this.leafTexture = leafTexture; 
		this.leafCount = leafCount;
		this.vertexBufferObjectManager = vertexBufferObjectManager;
		this.wind = wind;
		leaves = new Leaf[this.leafCount];
	}
	
	public void setLeavesFlyout() {
		TimerHandler tHandler = new TimerHandler(10, true, new ITimerCallback() {
			
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
				int randomLeaf = MathUtils.random(0, leafCount - 1);
				if(leaves[randomLeaf].getX() < Constants.SCENE_WIDTH) {
					leaves[randomLeaf].fly();
				}
			}
		});
		this.registerUpdateHandler(tHandler);
	}
	
	public void repopulateTree() {
		for(Leaf leaf: leaves) {
			if(leaf.getX() > Constants.SCENE_WIDTH) {
				leaf.setTreeBranch(getRandomTreeBranch());
			}
		}
	}
	
	public void treePopulate(Scene scene) {
		for(int i = 0; i < leafCount; i++) {
			final TreeBranch randomTreeBranch = getRandomTreeBranch();
			final LeafTexture randomLeafTexture = getRandomLeafTexture();
			leaves[i] = new Leaf(randomTreeBranch, randomLeafTexture, vertexBufferObjectManager, wind);
			leaves[i].setPosition(x + randomTreeBranch.x, y + randomTreeBranch.y);
			leaves[i].shake();
			scene.attachChild(leaves[i]);
		}
	}
	
	private LeafTexture getRandomLeafTexture() {
		final int random = MathUtils.random(0, leafTexture.length - 1);
		return leafTexture[random];
	}
	
	private TreeBranch getRandomTreeBranch() {
		final int random = MathUtils.random(0, treeBranch.length - 1);
		return treeBranch[random];
	}
	
	@Override
	public void setPosition(float pX, float pY) {
		this.x = pX;
		this.y = pY;
		for(Leaf leaf: leaves) {
			final TreeBranch leafTreeBranch = leaf.getTreeBranch();
			leaf.setPosition(x + leafTreeBranch.x, y + leafTreeBranch.y);
		}
		super.setPosition(pX, pY);
	}
	


}
