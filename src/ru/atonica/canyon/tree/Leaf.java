package ru.atonica.canyon.tree;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.CardinalSplineMoveModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.CardinalSplineMoveModifier.CardinalSplineMoveModifierConfig;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.ease.EaseCubicOut;
import org.andengine.util.modifier.ease.EaseSineInOut;
import org.andengine.util.modifier.ease.EaseSineOut;

import ru.atonica.canyon.Constants;
import ru.atonica.canyon.Wind;
import ru.atonica.canyon.Wind.WindDirection;

import android.util.Log;

public class Leaf extends Sprite {
	
	private float centerX, centerY;
	private float rotation;
	private float x, y;
	private TreeBranch treeBranch;
	private boolean flyout = false;

	public Leaf(TreeBranch treeBranch, LeafTexture leafTexture, VertexBufferObjectManager vertexBufferObjectManager, Wind wind) {
		super(treeBranch.x, treeBranch.y, leafTexture.textureRegion, vertexBufferObjectManager);
		this.treeBranch = treeBranch;
		this.centerX = leafTexture.centerX;
		this.centerY = leafTexture.centerY;
		setRotationCenter(centerX, centerY);
		setScaleCenter(centerX, centerY);
		if(wind.windDirection == WindDirection.RIGHT) {
			rotation = MathUtils.random(-60f, 60f);
		}
		setRotation(rotation);
		final float randomScale = MathUtils.random(0.2f, 0.5f); 
		setScale(randomScale);
		setPosition(treeBranch.x, treeBranch.y);
	}
	
	public void setTreeBranch(TreeBranch treeBranch) {
		this.treeBranch = treeBranch;
		setPosition(treeBranch.x, treeBranch.y);
	}
	
	@Override
	public void setPosition(float pX, float pY) {
		this.x = pX - centerX;
		this.y = pY - centerY;
		super.setPosition(this.x, this.y);
	}
	
	public TreeBranch getTreeBranch() {
		return treeBranch;
	}
	
	public void shake() {
		SequenceEntityModifier mod = getShakeModifier();
		registerEntityModifier(mod);
	}
	
	private SequenceEntityModifier getShakeModifier() {
		final float speed = MathUtils.random(20f, 50f);//40f, 60f
		final float scatter = MathUtils.random(10f, 20f);
		final float duration = scatter / speed;
		rotation = rotation<0?rotation:-rotation;
		SequenceEntityModifier mod = new SequenceEntityModifier(modListener,
				new RotationModifier(duration, rotation, rotation + scatter),
				new RotationModifier(duration * 2, rotation + scatter, rotation),
				new RotationModifier(duration / 2, rotation, rotation + scatter),
				new RotationModifier(duration, rotation + scatter, rotation)
//				new DelayModifier(MathUtils.random(0.2f, 0.4f))
				);
		mod.setAutoUnregisterWhenFinished(true);
		return mod;
	}
	
	IEntityModifierListener modListener = new IEntityModifierListener() {
		
		@Override
		public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
			
		}
		
		@Override
		public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
			if(flyout) {
				pItem.registerEntityModifier(getFlyModifier());
			}
			pItem.registerEntityModifier(getShakeModifier());
			
		}
	};
	
	private SequenceEntityModifier getFlyModifier() {
		SequenceEntityModifier mod = new SequenceEntityModifier(modListener,
				new ParallelEntityModifier(
						new CardinalSplineMoveModifier(6, getModConfig(getPath(x, y), 0)),
						new SequenceEntityModifier(
								new RotationModifier(2, rotation, rotation - 359),
								new RotationModifier(2, rotation, rotation - 359),
								new RotationModifier(2, rotation, rotation - 359)
								)
						)
				);
		
		mod.setAutoUnregisterWhenFinished(true);
		flyout = false;
		return mod;
	}
	
	public void fly() {
		flyout = true;
	}
	
	private CardinalSplineMoveModifierConfig getModConfig(float[][] points, float tension) {
		final int pointsCount = points.length;
		CardinalSplineMoveModifierConfig modConfig = new CardinalSplineMoveModifierConfig(pointsCount, tension);
		for(int i = 0; i < pointsCount; i++) {
			modConfig.setControlPoint(i, points[i][0], points[i][1]);
		}
		return modConfig;
	}
	
	private float[][] getPath(float startX, float startY) {
		final int step = 200;
		final int count = (int) ((Constants.SCENE_WIDTH + 200 - startX) / 100);
		final float[][] path = new float[count][2];
		float x = startX;
		float y = startY;
		for(int i = 0; i < count; i++) {
			path[i][0] = x;
			x += MathUtils.random(100f, 250f);
			path[i][1] = MathUtils.random(y - 5, y + 15);
			y += 50;
		}
		if(path[count-1][0] < Constants.SCENE_WIDTH) {
			path[count-1][0] = Constants.SCENE_WIDTH + 1;
		}
		return path;
	}
	
	

}
