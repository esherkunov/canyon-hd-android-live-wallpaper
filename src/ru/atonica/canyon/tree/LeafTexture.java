package ru.atonica.canyon.tree;

import org.andengine.opengl.texture.region.ITextureRegion;

public class LeafTexture {
	public ITextureRegion textureRegion;
	public float centerX;
	public float centerY;
	public LeafTexture(ITextureRegion textureRegion, float centerX,
			float centerY) {
		super();
		this.textureRegion = textureRegion;
		this.centerX = centerX;
		this.centerY = centerY;
	}
	
}
