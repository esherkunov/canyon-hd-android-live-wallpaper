package ru.atonica.canyon.tree;

public class TreeBranch {
	public float x;
	public float y;
	public TreeBranch(float x, float y) {
		super();
		this.x = x;
		this.y = y;
	}
	public static TreeBranch[] fromArray(float[][] branchArray) {
		final int arrLength = branchArray.length;
		TreeBranch[] treeBranch = new TreeBranch[arrLength];
		for(int i = 0; i < arrLength; i++) {
			treeBranch[i] = new TreeBranch(branchArray[i][0], branchArray[i][1]);
		}
		return treeBranch;
	}
	
}
