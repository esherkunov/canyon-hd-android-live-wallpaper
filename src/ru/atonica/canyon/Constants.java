package ru.atonica.canyon;

public class Constants {
	public static final boolean Debug = true;
	public static final String TAG = "Canyon";
	public static final float SCENE_WIDTH = 1920;
	public static final float SCENE_HEIGHT = 800;
}
