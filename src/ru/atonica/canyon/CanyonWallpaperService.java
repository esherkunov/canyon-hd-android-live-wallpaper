package ru.atonica.canyon;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.ui.livewallpaper.BaseLiveWallpaperService;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.bitmap.BitmapTextureFormat;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.util.GLState;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import ru.atonica.canyon.Wind.WindDirection;
import ru.atonica.canyon.tree.LeafTexture;
import ru.atonica.canyon.tree.Tree;


public class CanyonWallpaperService extends BaseLiveWallpaperService {
	
	private static final float SCENE_HEIGHT = Constants.SCENE_HEIGHT;
	private static final float SCENE_WIDTH = Constants.SCENE_WIDTH;
	public static final float SCENE_X = 0;
	public static final float SCENE_Y = 0;
	public static final float CAMERA_HEIGHT = SCENE_HEIGHT;
	
	private Camera mCamera;
	private float mXOffset = 0;
	private boolean mIsPreview = true;
	
	private BitmapTextureAtlas mTextureAtlas;	
	private ITextureRegion mBackgroundTexture;
	private ITextureRegion mLeftHillTexture, mRightHillTexture, mMiddleHillTexture;
	private ITextureRegion mStoneTexture;
	private ITiledTextureRegion mCloudTextures[];
	private ITextureRegion mLensFlareTexture, mDustTexture, mPlaneTexture, treeTexture; 
	private ITiledTextureRegion mBirdTexture;
	
	private Sprite background, leftHill, rightHill, middleHill, stone, lensflare, dust;
	private Plane plane;
	private LeafTexture[] leafTexture;
	private Tree tree;
	private final Scene scene = new Scene();
	private float mSceneOffset = 0f;

	@Override
	public EngineOptions onCreateEngineOptions() {
		final float width = getResources().getDisplayMetrics().widthPixels;
		final float height = getResources().getDisplayMetrics().heightPixels;
		final float idx = width / height;
		final float newWidth = SCENE_HEIGHT * idx;
		mCamera = new Camera(0f, 0f, newWidth, CAMERA_HEIGHT);
		return new EngineOptions(true, ScreenOrientation.LANDSCAPE_SENSOR, new RatioResolutionPolicy(newWidth, CAMERA_HEIGHT), mCamera);
	}

	@Override
	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws Exception {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		mTextureAtlas = new BitmapTextureAtlas( getTextureManager(),2048, 2048, BitmapTextureFormat.RGBA_8888, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "blank_2048x2048.png", 0, 0);
		mBackgroundTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "background.jpg", 5, 5);
		mLeftHillTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "hill_left.png", 5, 810);
		mRightHillTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "hill_right.png", 496, 810);
		mMiddleHillTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "hill_middle.png", 401, 810);
		mStoneTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "stone.png", 5, 997);
		treeTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "tree.png", 5, 1410);
		mCloudTextures = new ITiledTextureRegion[] {				
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mTextureAtlas, this, "cloud1.png", 1025, 1362, 1, 1),
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mTextureAtlas, this, "cloud2.png", 977, 1692, 1, 1),
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mTextureAtlas, this, "cloud3.png", 1025, 997, 1, 1),
		};
		mLensFlareTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "lensflare.png", 1905, 977);
		mBirdTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mTextureAtlas, this, "bird.png", 401, 972, 9, 1);
//		mDustTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "dust.png", 511, 1410);
		mPlaneTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "plane.png", 645, 810);
		leafTexture = new LeafTexture[] {
				new LeafTexture(BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "leaf3.png", 511, 1410), 0, 31),
				new LeafTexture(BitmapTextureAtlasTextureRegionFactory.createFromAsset(mTextureAtlas, this, "leaf2.png", 623, 1410), 0, 31)
		};
		mTextureAtlas.load();
		pOnCreateResourcesCallback.onCreateResourcesFinished();

	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
			throws Exception {
		VertexBufferObjectManager vertexBufferObjectManager = getVertexBufferObjectManager();
		
		// background >>
		background = new Sprite(0, 0, mBackgroundTexture, vertexBufferObjectManager);
		background.setIgnoreUpdate(true);
		background.setChildrenIgnoreUpdate(true);
		scene.attachChild(background);
		// << background
		
		// plane >>
		plane = new Plane(-mPlaneTexture.getWidth(), 250f, mPlaneTexture, vertexBufferObjectManager);
		scene.attachChild(plane);
		// << plane
		
		// >> middleHill
		middleHill = new Sprite(642, 286, mMiddleHillTexture, vertexBufferObjectManager);
		middleHill.setIgnoreUpdate(true);
		middleHill.setChildrenIgnoreUpdate(true);
		scene.attachChild(middleHill);
		// << middleHill

		// clouds >>
		final Cloud[] clouds = new Cloud[]{
				new Cloud(129, 0, mCloudTextures[0], vertexBufferObjectManager, 10f),
				new Cloud(257, 0, mCloudTextures[1], vertexBufferObjectManager, 7f),
				new Cloud(51, 0, mCloudTextures[2], vertexBufferObjectManager, 6f),
		};
		for(final Cloud cloud: clouds){
			scene.attachChild(cloud);
		}
		// << clouds
		
		// >> leftHill
		leftHill = new Sprite(28, 200, mLeftHillTexture, vertexBufferObjectManager);
		leftHill.setIgnoreUpdate(true);
		leftHill.setChildrenIgnoreUpdate(true);
		scene.attachChild(leftHill);
		// << leftHill
		
		// >> birds
		
		Bird masterBird = new Bird(SCENE_WIDTH, SCENE_HEIGHT, mBirdTexture, vertexBufferObjectManager);
		masterBird.setTag(0);
		scene.attachChild(masterBird);
		Bird[] birdMass = new Bird[7];
		for(int i = 0; i < birdMass.length; i++) {
			birdMass[i] = new Bird(SCENE_WIDTH, SCENE_HEIGHT, mBirdTexture, vertexBufferObjectManager, false);
			birdMass[i].setTag(i + 1);
			scene.attachChild(birdMass[i]);
		}
		masterBird.setFollowers(birdMass);
		masterBird.startBirds();
		
		// << birds
		
		// >> rightHill
		rightHill = new Sprite(958, 253, mRightHillTexture, vertexBufferObjectManager);
		rightHill.setIgnoreUpdate(true);
		rightHill.setChildrenIgnoreUpdate(true);
		scene.attachChild(rightHill);
		// << rightHill
		
		// >> dust
//		dust = new Sprite(720, 516 - mDustTexture.getHeight(), mDustTexture, vertexBufferObjectManager);
//		CubicBezierCurveMoveModifier dustModifier1 = new CubicBezierCurveMoveModifier(30, 
//				720, 516-mDustTexture.getHeight()/2, 
//				1340, 508-mDustTexture.getHeight()/2, 
//				1368, 465-mDustTexture.getHeight()/2, 
//				1417, 457-mDustTexture.getHeight()/2);
//		CubicBezierCurveMoveModifier dustModifier2 = new CubicBezierCurveMoveModifier(30, 
//				1419, 444-mDustTexture.getHeight()/2, 
//				1495, 411-mDustTexture.getHeight()/2, 
//				1749, 394-mDustTexture.getHeight()/2, 
//				SCENE_WIDTH, 394-mDustTexture.getHeight()/2);
//		dust.registerEntityModifier(new SequenceEntityModifier(dustModifier1, dustModifier2));
//		scene.attachChild(dust);
		// << dust
		
		// >> stone
		stone = new Sprite(0, 457, mStoneTexture, vertexBufferObjectManager);
		stone.setIgnoreUpdate(true);
		stone.setChildrenIgnoreUpdate(true);
		scene.attachChild(stone);
		// << stone
		
		// >> tree
		final Wind wind = new Wind(1f, WindDirection.RIGHT);
		tree = new Tree(350, 323, treeTexture, vertexBufferObjectManager, wind, leafTexture, 30);
//		tree.setIgnoreUpdate(true);
//		tree.setChildrenIgnoreUpdate(true);
		tree.treePopulate(scene);
		tree.setLeavesFlyout();
		scene.attachChild(tree);
		// << tree
		
		// lensflare >>
		lensflare = new Sprite(1340 - mLensFlareTexture.getWidth() / 2, 335, mLensFlareTexture, vertexBufferObjectManager);
		lensflare.setIgnoreUpdate(true);
		lensflare.setChildrenIgnoreUpdate(true);
		lensflare.setRotationCenter(mLensFlareTexture.getWidth() / 2, 5);
		lensflare.setRotation(20f);
		scene.attachChild(lensflare);
		// << lensflare
		
		pOnCreateSceneCallback.onCreateSceneFinished(scene);
	}

	@Override
	public void onSurfaceChanged(GLState pGLState, int pWidth, int pHeight) {
		pGLState.enableDither();
		final float idx = (float)pWidth / pHeight;
		final float width = SCENE_HEIGHT * idx;
		mCamera.set(0, 0, width, SCENE_HEIGHT);
		
		if(mIsPreview) {
			mCamera.setCenter(SCENE_WIDTH / 2, mCamera.getCenterY());
		}
		else {
			final float offset = SCENE_WIDTH * mXOffset - mCamera.getWidth() * mXOffset;
			mSceneOffset = offset;
			mCamera.setCenter(offset + mCamera.getWidth() / 2, mCamera.getCenterY());
		}
		setParallax();
			
		super.onSurfaceChanged(pGLState, pWidth, pHeight);
	}
	
	@Override
	protected void onOffsetsChanged(float pXOffset, float pYOffset,
			float pXOffsetStep, float pYOffsetStep, int pXPixelOffset,
			int pYPixelOffset) {
		if(pXOffsetStep != 0f) {
			mIsPreview = false;
		}
		else {
			mIsPreview = true;
		}
		mXOffset = pXOffset;
		final float offset = SCENE_WIDTH * pXOffset - mCamera.getWidth() * pXOffset;
		mSceneOffset = offset;
		mCamera.setCenter(offset + mCamera.getWidth() / 2, mCamera.getCenterY());
		setParallax();
		
		super.onOffsetsChanged(pXOffset, pYOffset, pXOffsetStep, pYOffsetStep,
				pXPixelOffset, pYPixelOffset);
	}
	
	@Override
	protected synchronized void onResume() {
		if(tree != null) {
			tree.repopulateTree();
		}
		super.onResume();
	}
	
	private void setParallax() {
		if(scene != null) {
			if(lensflare != null) {
				final float grad = (1 -mXOffset) * 70 + 15;
				lensflare.setRotation(grad);
			}
			if(stone != null) {
				final float stoneX = SCENE_WIDTH * (0.5f - mXOffset) * (-0.05f);
				stone.setPosition(stoneX, 457);
			}
			if(tree != null) {
				final float treeX = SCENE_WIDTH * (0.5f - mXOffset) * (-0.1f);
				tree.setPosition(350 + treeX, 323);
			}
		}
	}
	
	@Override
	public void onPopulateScene(Scene pScene,
			OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}
	
	private class Cloud extends AnimatedSprite {
		private final PhysicsHandler mPhisicsHandler;

		public Cloud(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, float pVelocityX) {
			super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
			mPhisicsHandler = new PhysicsHandler(this);
			this.registerUpdateHandler(mPhisicsHandler);
			mPhisicsHandler.setVelocity(pVelocityX, 0f);
		}
		
		@Override
		protected void onManagedUpdate(float pSecondsElapsed) {
			if(this.mX > SCENE_WIDTH) {
				this.mX = -this.getWidth();
			}
			super.onManagedUpdate(pSecondsElapsed);
		}
	}

}
