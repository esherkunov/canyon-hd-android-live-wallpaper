package ru.atonica.canyon;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;
import org.andengine.util.modifier.IModifier;

public class Plane extends Sprite {
	
	private float textureWidth, textureHeight;

	public Plane(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObject) {
		super(pX, pY, pTextureRegion, pVertexBufferObject);
		textureHeight = pTextureRegion.getHeight();
		textureWidth = pTextureRegion.getWidth();
		this.registerEntityModifier(getModifier(enModListener));
	}
	
	private SequenceEntityModifier getModifier(IEntityModifierListener modListener) {
		
		return getPath0(modListener);
	}
	
	private SequenceEntityModifier getPath0(IEntityModifierListener modListener) {
		final float startX = -872f,
				startY = 338f,
				endX = 2084f,
				endY = -138f;
		final double planePath = (float) Math.sqrt(startY * startY + endX * endX);
		final double planeAngle = Math.toDegrees((Math.abs(endY) + Math.abs(startY)) / planePath);
		this.setRotation((float)-planeAngle);
		SequenceEntityModifier eMod = new SequenceEntityModifier(
				new DelayModifier(MathUtils.random(10, 20)),
				new MoveModifier(360, startX, endX, startY, endY)
				);
		eMod.setAutoUnregisterWhenFinished(true);
		eMod.addModifierListener(enModListener);
		return eMod;
	}
	
	private SequenceEntityModifier getPath1(IEntityModifierListener modListener) {
		final float startX = 900f,
				startY = 0f,
				endX = 1538f,
				endY = 1260f;
		final double planePath = (float) Math.sqrt(startY * startY + endX * endX);
		final double planeAngle = Math.toDegrees((endY - startY) / planePath);
		this.setRotation((float)planeAngle);
		SequenceEntityModifier eMod = new SequenceEntityModifier(
				new DelayModifier(MathUtils.random(1, 2)),
				new MoveModifier(180, startX, endX, startY, endY)
				);
		eMod.setAutoUnregisterWhenFinished(true);
		eMod.addModifierListener(enModListener);
		return eMod;
	}
	
	IEntityModifierListener enModListener = new IEntityModifierListener() {
		
		@Override
		public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
			
		}
		
		@Override
		public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
			pItem.registerEntityModifier(getModifier(this));
		}
	};
	
	

}
