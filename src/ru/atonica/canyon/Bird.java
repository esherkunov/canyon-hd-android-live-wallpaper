package ru.atonica.canyon;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.CardinalSplineMoveModifier;
import org.andengine.entity.modifier.CardinalSplineMoveModifier.CardinalSplineMoveModifierConfig;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;
import org.andengine.util.modifier.IModifier;

import android.util.Log;

public class Bird extends AnimatedSprite {
	
	public static final String TAG = "Canyon";
	
	private float textureWidth, textureHeight;
	private Bird[] followers = {};
	private boolean master = false;
	private int pathNumber = 0;
	private int birdsDelay = MathUtils.random(3, 10);
	
	public Bird(float pX, float pY, ITiledTextureRegion pTiledTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager) {
		this(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, true);
	}
	
	public Bird(float pX, float pY, ITiledTextureRegion pTiledTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager, boolean master) {
		super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
		textureWidth = pTiledTextureRegion.getWidth();
		textureHeight = pTiledTextureRegion.getHeight();
		this.animate(MathUtils.random(105, 135));
		this.master = master;
	}
	
	public void setFollowers(Bird[] birds) {
		followers = birds;
	}
	
	public void startBirds() {
		if(master) {
			this.registerEntityModifier(getModifier(enModListener));
		}
	}
	
	@Override
	public void registerEntityModifier(IEntityModifier pEntityModifier) {
		for(Bird bird: followers) {
			bird.registerEntityModifier(getModifier(null));
		}
		super.registerEntityModifier(pEntityModifier);
		if(Constants.Debug) {
			Log.i(TAG, "modifier registered for: " + master + " modCount: " + this.getEntityModifierCount());
		}
	}
	
	private CardinalSplineMoveModifierConfig getModConfig(float[][] points, float tension) {
		final int pointsCount = points.length;
		CardinalSplineMoveModifierConfig modConfig = new CardinalSplineMoveModifierConfig(pointsCount, tension);
		for(int i = 0; i < pointsCount; i++) {
			modConfig.setControlPoint(i, points[i][0], points[i][1]);
		}
		return modConfig;
	}
	
	private float[][] randomizePoints(float[][] points) {
		final int pointsCount = points.length;
		float[][] newPoints = new float[pointsCount][2];
		for(int i = 0; i < pointsCount; i++) {
			newPoints[i][0] = points[i][0] + MathUtils.random(-25f, 25f);
			newPoints[i][1] = points[i][1] + MathUtils.random(-15f, 15f);
		}
		return newPoints;
	}
	
	private SequenceEntityModifier getPath0(IEntityModifierListener modListener, int startDelay) {
		float[][] points = {
				{1338, 314},
				{1260, 314},
				{1200, 314},
				{1100, 317},
				{1036, 317},
				{930, 317},
				{830, 320},
				{695, 320},
				{540, 305},
				{400, 290},
				{200, 260},
				{0, 245},
				{-250, 230}
		};
		this.setScale(0.1f);
		CardinalSplineMoveModifierConfig modConfig = getModConfig(randomizePoints(points), 0);
		SequenceEntityModifier eMod = new SequenceEntityModifier(
				new DelayModifier(startDelay), 
				new ParallelEntityModifier(
						new ScaleModifier(46, 0.1f, 0.5f),
						new CardinalSplineMoveModifier(50, modConfig)
						)
				);
		if(modListener != null) {
			eMod.addModifierListener(modListener);
		}
		eMod.setAutoUnregisterWhenFinished(true);
		return eMod;
	}
	
	private SequenceEntityModifier getPath1(IEntityModifierListener modListener, int startDelay) {
		float [][] points = {
				{423, 800},
				{591, 729},
				{725, 644},
				{776, 536},
				{793, 481},
				{800, 469},
				{811, 440},
				{810, 421},
				{810, 403},
				{809, 389},
				{809, 376},
				{809, 370},
				{808, 365}
		};
		this.setScale(0.8f);
		this.setPosition(423, 800);
		CardinalSplineMoveModifierConfig modConfig = getModConfig(randomizePoints(points), 0);
		SequenceEntityModifier eMod = new SequenceEntityModifier(
				new DelayModifier(startDelay), 
				new ParallelEntityModifier(
						new SequenceEntityModifier(
							new ScaleModifier(10, 0.8f, 0.5f),
							new ScaleModifier(7, 0.5f, 0.1f)
							),
						new CardinalSplineMoveModifier(17, modConfig)
						)
				);
		if(modListener != null) {
			eMod.addModifierListener(modListener);
		}
		eMod.setAutoUnregisterWhenFinished(true);
		return eMod;
	}
	
	private SequenceEntityModifier getModifier(IEntityModifierListener modListener) {
		if(modListener != null) { //then it is master bird
			pathNumber = MathUtils.random(0, 1);
			birdsDelay = MathUtils.random(10, 20);
			if(Constants.Debug) {
				Log.i(TAG, "new path generated: " + pathNumber);
			}
		}
		switch(pathNumber) {
			case 0:
				return getPath0(modListener, birdsDelay);
			case 1:
				return getPath1(modListener, birdsDelay);
			default:
				return getPath0(modListener, birdsDelay);
		}
	}
	
	IEntityModifierListener enModListener = new IEntityModifierListener() {
		
		@Override
		public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
			
		}
		
		@Override
		public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
			pItem.registerEntityModifier(getModifier(this));
		}
	};
	
	private float lastX = this.getX();

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		if(lastX < this.getX()) {
			this.setFlippedHorizontal(true);
		} else {
			this.setFlippedHorizontal(false);
		}
		lastX = this.getX();
		super.onManagedUpdate(pSecondsElapsed);
	}
}
